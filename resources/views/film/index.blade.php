@extends('layout.master')
@section('judul')
    Halaman Tambah Film
@endsection
@section('isi')
@auth
<a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>

@endauth
    <div class="row">
        @foreach ($film as $item)
        <div class="col-4">
            <div class="card">
                <img class="card-img-top" src="{{asset('poster/'.$item->poster)}}" alt="Card image cap" style="widht:300px; height:400px">
                <div class="card-body">
                  <h2 class="card-title">{{$item->judul}} ({{$item->tahun}})</h2>
                  <p class="card-text">{{Str::limit($item->ringkasan,50)}}</p>
                  @auth
                  <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                  <a href="/film/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                  <form action="/film/{{$item->id}}" method="POST" style="display:inline">
                    @csrf
                    @method('delete')
                    <input type="submit" value="delete" class="btn btn-danger">
                </form> 
                @endauth
                @guest
                <a href="/film/{{$item->id}}" class="btn btn-primary">Detail</a>
                @endguest
                </div>
              </div>
        </div>
        @endforeach
    </div>
        @endsection