@extends('layout.master')
@section('judul')
    Halaman List Genre
@endsection
@section('isi')

        <form action="/film" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Judul Film</label>
                <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Title">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Ringkasan Film</label>
                <textarea name="ringkasan" class="form-control" id="ringkasan" cols="30" rows="10"></textarea>
                @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Tahun Rilis</label>
                <input type="text" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Title">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Genre Film</label>
                <select class="form-control" name="genre_id" id="genre_id">
                    <option value="">--Pilih Genre--</option>
                    @foreach ($genre as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endforeach
                </select>
                @error('genre_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Poster Film</label></label>
                <input type="file" class="form-control" name="poster" id="poster" placeholder="Masukkan Title">
                {{-- @error('poster')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror  --}}
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection