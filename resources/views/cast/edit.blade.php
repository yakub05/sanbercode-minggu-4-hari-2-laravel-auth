@extends('layout.master')
@section('judul')
    Halaman Edit Cast berid {{$cast->id}}
@endsection
@section('isi')

        <form action="/cast/{{$cast->id}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Nama Cast</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

            </div>
            <div class="form-group">
                <label for="title">Umur Cast</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="title" placeholder="Masukkan Title">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Bio Cast</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="title" placeholder="Masukkan Title">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                
            <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
        </form>
@endsection