<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
          <img src="{{ asset('template/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
              alt="User Image">
      </div>
      <div class="info">
        @auth
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
          @endauth
          @guest
              <p>Belum Login</p>
          @endguest
      </div>
  </div>

  <!-- SidebarSearch Form -->
  <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
              <button class="btn btn-sidebar">
                  <i class="fas fa-search fa-fw"></i>
              </button>
          </div>
      </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           @auth
          <li class="nav-item">
              <a href="/" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                      Dashboard
                  </p>
              </a>
          </li>
          
        @endauth


        <li class="nav-item">
            <a href="/film" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Film
                </p>
            </a>
        </li>
          @auth 

          <li class="nav-item bg-danger">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <i class="nav-icon fa fa-table"></i>
                  <p>{{ __('Logout') }}</p>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
        </li>
        @endauth
        @guest
        <li class="nav-item">
            <a href="/login" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
                <p>Login</p>
            </a>
        </li>
        @endguest
      </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>